(defun guix-githash (url tag)
  "Capture guix-ha.sh output and insert the result at point."
  (interactive "sURL:\nsTag:")
  (insert
   (shell-command-to-string
    ;; Change path below to script location.
    (concat "./guix-ha.sh " url " " tag))))

(defun guix-import (importer package)
  "Import guix package and insert into Emacs. Currently only PyPI and Cargo
are supported."
  (interactive "sImporter:\nsPackage:")
  (setq importer-list
        '(("pypi" . "python")
	  ("cargo" . "rust")))
  (let ((cmd (concat "guix import " importer " " package " 2>/dev/null")))
    (cond ((string= importer "pypi")
	   (insert
	    (concat
	     "(define-public " (cdr (assoc importer importer-list)) "-" package
	     "\n" (shell-command-to-string cmd) ")")))
	   ((string= importer "crate")
	    (insert
	     (shell-command-to-string cmd)))
	   (t (error "Invalid importer.")))))

(defun guix-copyright ()
  "Insert a copyright header."
  (interactive)
  (insert ";;; Copyright © year name <email>"))

(defun guix-template ()
  "Template for a git-based fetch."
  (interactive)
  (insert
   "(define-public
  (package
    (name \"\")
    (version \"\")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url \"\")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 \"\"))))
    (build-system -build-system)
    (home-page \"\")
    (synopsis \"\")
    (description
     \"\")
    (license license:)))"))
