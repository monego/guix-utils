#!/bin/sh

# Usage: ./guix-ha.sh URL BRANCH/TAG

cd /tmp
reponame="${1##*/}"

if [ ! -d $reponame ]
then
    git clone --single-branch --depth 1 --branch $2 $1 --quiet 2>/dev/null
    cd $reponame
else
    cd $reponame
    git checkout $2 --quiet
fi

guix hash -rx . | tr -d '\n'
